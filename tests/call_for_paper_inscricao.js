/* globals gauge*/
"use strict";
const path = require('path');
const { 
	openBrowser,
	goto,
	click,
	near,
	link,
	screenshot,
	closeBrowser
} = require('taiko');
const assert = require("assert");
const { table } = require('console');
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({
        headless: headless
    })
});

afterSuite(async () => {
    await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
    const screenshotFilePath = path.join(process.env['gauge_screenshots_dir'],
        `screenshot-${process.hrtime.bigint()}.png`);

    await screenshot({
        path: screenshotFilePath
    });
    return path.basename(screenshotFilePath);
};
step("Verificar botão cadastro palestras", async function() {
	//throw 'Unimplemented Step';
	await goto("https://thedevconf.com/tdc/2022/future/");
});

step("Acessar o link de inscrição da cidade e verificar o status <table>", async function(table) {
	
	//for (var row of table.rows) {
        await click('Call4Papers');

		let data = new Date();
		let dataFormatada = ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + (data.getFullYear()); 
		let dataFormatadaDate = new Date();

		let dataStringExpired = "25/09/2022";
		let datasplited = dataStringExpired.split("/");
		let dataString = new Date(datasplited[2]+"/"+datasplited[1]+"/"+datasplited[0])
		
		if (dataFormatadaDate < dataString) {
			await link("SUBMETA SUA PALESTRA", near(": 25/09/2022")).exists();
			console.log("submeta")
		} else {
			console.log("encerrado")
			await link("CFP ENCERRADO", near(": 25/09/2022")).exists();

    	}
});